package com.business;

import com.entity.Student;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MyPC
 */
public interface Studentbusiness {
    public boolean createStudent(Student student);
    public boolean updateStudent(Student student);
    public Student getStudent(long id);
    public boolean deleteStudent(Student student);
}
