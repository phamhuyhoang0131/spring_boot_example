package com.business;

import com.dao.StudentDao;
import com.entity.Student;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MyPC
 */
@Service
public class StudentBusinessImpl implements Studentbusiness{
    @Autowired
    private StudentDao Dao;
    @Override
    public boolean createStudent(Student student){
        try {
            Dao.save(student);
            return true;
        } catch (Exception e) {
            System.out.println("Error when insert new student");
            System.out.println(e.toString());
            return false;
        }
    }
    @Override
    public boolean updateStudent(Student student){
        try {
            Dao.save(student);
            return true;
        } catch (Exception e) {
            System.out.println("Error when insert new student");
            System.out.println(e.toString());
            return false;
        }
    }

    @Override
    @Transactional
    public Student getStudent(long studentID) {
            System.out.println("Tham so businness nhan duoc  : " + studentID);
            System.out.println(Dao.findOne(studentID));
            Student student = Dao.findOne(studentID);
            return student;
    }

    @Override
    public boolean deleteStudent(Student student) {
        try {
            Dao.delete(student);
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return false;
    }
}
