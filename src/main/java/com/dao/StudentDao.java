/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entity.Student;
import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

/**
 * A DAO for the entity Student is simply created by extending the CrudRepository
 interface provided by spring. The following methods are some of the ones
 * available from such interface: save, delete, deleteAll, findOne and findAll.
 * The magic is that such methods must not be implemented, and moreover it is
 * possible create new query methods working only by defining their signature!
 * 
 * @author hoangdev
 */
@Transactional
public interface StudentDao extends CrudRepository<Student, Long> {

  /**
   * Return the user having the passed name or null if no user is found.
   * 
   * @param name the user name.
   */
  public Student findByName(String name);

} // class StudentDao