package com.example;

import com.business.StudentBusinessImpl;
import com.controller.MainController;
import com.dao.StudentDao;
import com.entity.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
@SpringBootApplication
@EnableAutoConfiguration
@Configuration
@ComponentScan(basePackageClasses=MainController.class)
@ComponentScan(basePackageClasses = StudentBusinessImpl.class)
@EntityScan(basePackageClasses = Student.class)
@EnableJpaRepositories(basePackageClasses = StudentDao.class)
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
