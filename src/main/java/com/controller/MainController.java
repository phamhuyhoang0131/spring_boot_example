package com.controller;

import com.business.StudentBusinessImpl;
import com.dao.StudentDao;
import com.entity.Student;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author HoangDev
 */
@Controller
public class MainController {

    @Autowired
    StudentDao business;

    @RequestMapping(value = "/")
    public ResponseEntity<String> hello() {
        return new ResponseEntity<String>("Hello World!", HttpStatus.OK);
    }

    @RequestMapping(value = "/student/add")
    public ResponseEntity<String> addStudent() {
        // create new student object
        Student student = new Student();
        // add more properties to Student 
        student.setName("Hoang DEV");
        student.setAddress("Ha Noi");
        // save student to database
        try {
            business.save(student);
            return new ResponseEntity<String>("Student inserted!", HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.toString());
            return new ResponseEntity<String>("Could't insert student!", HttpStatus.EXPECTATION_FAILED);

        }
    }

    @RequestMapping(value = "/student/update")
    public ResponseEntity<String> updateStudent() {
        StudentBusinessImpl business = new StudentBusinessImpl();
        Student student = new Student();
        // add more properties to Student 
        // modify student have id = 1
        student.setId(1);
        student.setName("Hoang Student");
        student.setAddress("Ha Noi");
        business.createStudent(student);
        return new ResponseEntity<String>("Student updated!", HttpStatus.OK);
    }

    @RequestMapping(value = "/student")
    public ResponseEntity<String> getStudent(@RequestParam("id") long id) {
        System.out.println("Tham so nhan vao :" +id);
        Gson jsonPaser = new Gson();
        StudentBusinessImpl businessImpl = new StudentBusinessImpl();
        String studentJson = jsonPaser.toJson(businessImpl.getStudent(id));
        return new ResponseEntity<String>(studentJson,HttpStatus.OK);
    }
}
